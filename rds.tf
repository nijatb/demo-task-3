resource "aws_db_instance" "petclinic_db" {
  allocated_storage = 10
  #   apply_immediately    = true 
  publicly_accessible    = false
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "8.0.20"
  instance_class         = "db.t2.micro"
  identifier             = var.DB_NAME
  name                   = var.DB_NAME
  username               = var.DB_USERNAME
  password               = var.DB_PASS
  skip_final_snapshot    = true
  db_subnet_group_name   = "petclinic_db_subnet_group"
  vpc_security_group_ids = [aws_security_group.petclinic_db_sg.id]
  tags = {
    Owner = "NicatB"
    Name  = "petclinic_db"
  }
  depends_on = [aws_db_subnet_group.petclinic_db_subnet_group]
}

resource "aws_db_subnet_group" "petclinic_db_subnet_group" {
  name       = "petclinic_db_subnet_group"
  subnet_ids = [aws_subnet.petclinic_db_subnet_1.id, aws_subnet.petclinic_db_subnet_2.id]
  depends_on = [aws_subnet.petclinic_db_subnet_1, aws_subnet.petclinic_db_subnet_2]

  tags = {
    Name  = "petclinic_db_subnet_group"
    Owner = "NicatB"
  }
}

output "rds_address" {
  value = aws_db_instance.petclinic_db.address
}

