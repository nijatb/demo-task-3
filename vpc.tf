# VPC
resource "aws_vpc" "petclinic_vpc_NB" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name  = "petclinic_vpc_NB"
    Owner = "NicatB"
  }
}

# APP Subnet(With access to the Internet)
resource "aws_subnet" "petclinic_app_subnet" {
  vpc_id                  = aws_vpc.petclinic_vpc_NB.id
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = true
  tags = {
    Owner = "NicatB"
    Name  = "petclinic_app_subnet"
  }
  depends_on = [aws_vpc.petclinic_vpc_NB]
}

# DB Subnet #1 (Without access to the Internet)
resource "aws_subnet" "petclinic_db_subnet_1" {
  vpc_id            = aws_vpc.petclinic_vpc_NB.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-2a"
  tags = {
    Owner = "NicatB"
    Name  = "petclinic_db_subnet_1"
  }
  depends_on = [aws_vpc.petclinic_vpc_NB]
}

# DB Subnet #2 (Without access to the Internet)
resource "aws_subnet" "petclinic_db_subnet_2" {
  vpc_id            = aws_vpc.petclinic_vpc_NB.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-2b"
  tags = {
    Owner = "NicatB"
    Name  = "petclinic_db_subnet_2"
  }
  depends_on = [aws_vpc.petclinic_vpc_NB]
}

# APP Internet Gateway
resource "aws_internet_gateway" "petclinic_internet_gateway" {
  vpc_id = aws_vpc.petclinic_vpc_NB.id
  tags = {
    Owner = "NicatB"
    Name  = "petclinic_internet_gateway"
  }
  depends_on = [aws_vpc.petclinic_vpc_NB]
}

# APP Route to the Internet
resource "aws_route_table" "petclinic_app_route_to_internet" {
  vpc_id = aws_vpc.petclinic_vpc_NB.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.petclinic_internet_gateway.id
  }

  tags = {
    Owner = "NicatB"
    Name  = "petclinic_app_route_to_internet"
  }
  depends_on = [aws_vpc.petclinic_vpc_NB, aws_subnet.petclinic_app_subnet]
}

# Associate to APP Subnet
resource "aws_route_table_association" "route_for_app_subnet" {
  subnet_id      = aws_subnet.petclinic_app_subnet.id
  route_table_id = aws_route_table.petclinic_app_route_to_internet.id
  depends_on     = [aws_route_table.petclinic_app_route_to_internet]
}

resource "aws_security_group" "petclinic_app_sg" {
  name        = "petclinic_app_sg"
  description = "allow inbound access to the APP instances"
  vpc_id      = aws_vpc.petclinic_vpc_NB.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    description = "App Port"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "petclinic_db_sg" {
  name        = "petclinic_db_sg"
  description = "allow inbound access to the DB instances"
  vpc_id      = aws_vpc.petclinic_vpc_NB.id

  ingress {
    from_port   = 0
    to_port     = 3306
    protocol    = "tcp"
    description = "MYSQL"
    cidr_blocks = ["10.0.0.0/16"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
